Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: mupen64plus-audio-sdl
Upstream-Contact: mupen64plus@googlegroups.com
Source: https://github.com/mupen64plus/mupen64plus-audio-sdl/

Files: *
Copyright: 2009, Richard 'Richard42' Goedeken <Richard@fascinationsoftware.com>
License: GPL-2+

Files: debian/*
Copyright: 2009-2022, Sven Eckelmann <sven@narfation.org>
           2009-2022, Tobias Loose <TobiasLoose@gmx.de>
License: GPL-2+

Files: projects/unix/Makefile
Copyright: 2007-2008, Scott 'Tillin9' Knauert
  2007-2009, Richard 'Richard42' Goedeken <Richard@fascinationsoftware.com>
License: GPL-2+

Files: src/main.c
Copyright: 2007-2009, Richard 'Richard42' Goedeken <Richard@fascinationsoftware.com>
  2007, 2008, James 'Ebenblues' Hood
  2003, Juha 'JttL' Luotio
  2002, Hacktarux
License: GPL-2+

Files: src/main.h
Copyright: 2008-2012, Scott 'Tillin9' Knauert
  2008-2012, Richard 'Richard42' Goedeken <Richard@fascinationsoftware.com>
License: GPL-2+

Files: src/volume.c
  src/volume.h
Copyright: 2007-2008, Richard 'Richard42' Goedeken <Richard@fascinationsoftware.com>
  2007-2008, James 'Ebenblues' Hood
  2002, Hacktarux
License: GPL-2+

Files: src/circular_buffer.c
  src/circular_buffer.h
Copyright: 2015, Bobby Smiles
License: GPL-2+

Files: src/resamplers/*
  src/sdl_backend.c
  src/sdl_backend.h
Copyright: 2017, Bobby Smiles
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.
