#!/usr/bin/make -f
# -*- makefile -*-

-include /usr/share/dpkg/buildtools.mk

export PKG_CONFIG ?= pkg-config
export DEB_BUILD_MAINT_OPTIONS=hardening=+all,-pie optimize=+lto abi=+lfs

# mupen64plus and its plugins sometimes use signed integer overflows
# the compiler must not optimize them away due to their "undefined behavior"
DEB_CFLAGS_MAINT_APPEND=-fno-aggressive-loop-optimizations -fno-strict-overflow

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
DEB_HOST_GNU_CPU ?= $(shell dpkg-architecture -qDEB_HOST_GNU_CPU)
DEB_HOST_ARCH_OS ?= $(shell dpkg-architecture -qDEB_HOST_ARCH_OS)
MAKEOPTIONS = V=1 UNAME='$(DEB_HOST_ARCH_OS)' HOST_CPU='$(DEB_HOST_GNU_CPU)' APIDIR=/usr/include/mupen64plus/ DEBUG=1 PREFIX=/usr/ LIBDIR="/usr/lib/$(DEB_HOST_MULTIARCH)" PIC=1 OPTFLAGS="-DNDEBUG" NO_OSS=1 SDL_CONFIG=sdl2-config

binary binary-arch binary-indep build build-arch build-indep clean install install-arch install-indep:
	dh $@ --sourcedirectory="projects/unix"

override_dh_auto_test:
	# otherwise dh_auto_test fails with debhelper 9.20130624

override_dh_auto_clean:
	dh_auto_clean -- $(MAKEOPTIONS)

override_dh_auto_build:
	dh_auto_build -- all $(MAKEOPTIONS)

override_dh_auto_install:
	dh_auto_install -- $(MAKEOPTIONS)

override_dh_installchangelogs:
	dh_installchangelogs RELEASE

.PHONY: binary binary-arch binary-indep build build-arch build-indep clean install install-arch install-indep \
	override_dh_auto_clean override_dh_auto_test override_dh_auto_build override_dh_auto_install override_dh_installchangelogs
